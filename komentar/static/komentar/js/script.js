$(document).ready(function(){

    $('#search-box').keyup(function(){
        var judul = $('#komentar-title').text();
        $('div.komentar-box').css('background-color', '#DEEFF7');
        $.ajax({
            type: 'GET',
            data: {'keyword': $('#search-box').val(), 'judul': judul.substring(20, judul.length - 1)},
            url: 'search_komentar/',
            success: function(response){
                $.each(response.items, function(i, val){
                    var id = val.id;
                    $('#komentar-box-' + id).css('background-color', 'lightgreen');
                })
            },
            error: function(){
                console.log('Something went wrong');
            }
        })
    })

    $('#komentar-form').submit(function(e){
        e.preventDefault();
        var nama = $('.nav-label').text();
        var isi = $('#komentar-input').val();
        var judul = $('#komentar-title').text();
        $.ajax({
            type: 'POST',
            data: {'nama': nama.substring(4), 'isi': isi, 'judul': judul.substring(20, judul.length - 1), 'csrfmiddlewaretoken': CSRF_TOKEN},
            url: 'add_komentar/',
            success: function(response){
                var new_id = 'komentar-box-' + response.id;

                var clone = $("#template-result").contents().clone(true);
                clone.find('span').text('~' + response.nama);
                clone.find('div.komentar-isi').text(response.isi);
                clone.find('i').text(response.tanggal);
                clone.attr("id", new_id);
                $('div.main-komentar').append(clone);

                $('#komentar-kosong').remove();
                $('#komentar-input').val('');
            },
            error: function(){
                console.log('Something went wrong');
            }
        })
    })
    
})
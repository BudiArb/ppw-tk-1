from django.urls import path
from . import views

app_name = 'komentar'

urlpatterns = [
    path('<str:judul>', views.index_komentar, name='index_komentar'),
    # path('tambah_komentar/<str:judul>/<str:name>', views.tambah_komentar, name='tambah_komentar'),
    path('search_komentar/', views.search_komentar, name='search_komentar'),
    path('add_komentar/', views.add_komentar, name='add_komentar'),
]

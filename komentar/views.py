from .forms import Komen_Form
from .models import Komentar
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from jadwal.models import Jadwal

@login_required
def index_komentar(request, judul):
    jadwal = Jadwal.objects.get(judul=judul)
    if (request.user not in jadwal.users.all()):
        return redirect('main:home')

    response = {
        'komen_form': Komen_Form,
        'jadwal': jadwal
    }
    if Komentar.objects.all() is not None:
        response['daftar_komentar'] = Komentar.objects.all()
    return render(request, 'komentar/komentar.html', response)

### CURRENTLY USING AJAX, NOT MANUAL POST ANYMORE ###
# @login_required
# def tambah_komentar(request, judul, name):
#     jadwal = Jadwal.objects.get(judul=judul)
#     if (request.user not in jadwal.users.all()):
#         return redirect('main:home')

#     form = Komen_Form(request.POST or None)
#     if (form.is_valid() and request.method == 'POST'):
#         Komentar.objects.create(nama=name, isi=request.POST['isi'], jadwal=jadwal)
#     return HttpResponseRedirect('/komentar/' + judul)

@login_required
def search_komentar(request):
    keyword_ajax = request.GET.get('keyword', None)
    judul_ajax = request.GET.get('judul', None)
    current_jadwal = Jadwal.objects.get(judul=judul_ajax)
    komentar_list = Komentar.objects.all()
    data = {'items': []}
    for komentar_obj in komentar_list:
        if (komentar_obj.jadwal.judul == current_jadwal.judul and komentar_obj.nama == keyword_ajax):
            item = {'id': komentar_obj.pk}
            item['isi'] = komentar_obj.isi
            data['items'].append(item)
    return JsonResponse(data, status=200)

@login_required
def add_komentar(request):
    form = Komen_Form(request.POST or None)
    data = {}
    if (form.is_valid() and request.method == 'POST'):
        new_komentar = Komentar.objects.create(nama=request.POST['nama'], isi=request.POST['isi'], jadwal=Jadwal.objects.get(judul=request.POST['judul']))
        tanggal = new_komentar.tanggal.strftime('%b, %#d %Y, %#I:%M %p')
        if (tanggal[-2:] == 'AM'):
            tanggal = tanggal[:-2] + 'a.m.'
        else:
            tanggal = tanggal[:-2] + 'p.m.'
        data['id'] = new_komentar.pk
        data['nama'] = new_komentar.nama
        data['isi'] = new_komentar.isi
        data['tanggal'] = tanggal
    return JsonResponse(data, status=200)
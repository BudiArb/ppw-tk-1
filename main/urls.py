from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('visitor_log.json', views.visitor_log, name='visitor_log'),
    path('add_visitor', views.add_visitor, name='add_visitor'),
]

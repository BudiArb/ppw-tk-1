$(document).ready(function(){
    $("#view-content").hide();
    refresh_visitors_log();

    $("#view-icon").click(function(){
        $("#view-content").toggle();
    });

    $("#visitor-form").submit(function(event){
        event.preventDefault();

        var form = $(this);
        var formAction = form.attr("action");
        var formMethod = form.attr("method");
        var formData = form.serialize();
        $.ajax({
            url: formAction,
            type: formMethod,
            data: formData,
            success: function(data){
                refresh_visitors_log();
                form.trigger("reset");
            }
        });
    });

    function refresh_visitors_log(){
        var table = $("#visitor-table");
        table.empty();
        $.getJSON("/visitor_log.json", function(data){
            var visitors = data.visitors;
            console.log(visitors);
            var len = visitors.length;
            if (len == 0) {
                var col = $("<td colspan='2'/>");
                col.text("Belum ada kota terdaftar").html();
                var row = $("<tr/>");
                row.append(col);
                table.append(row);
            } else {
                for (i = 0; i < len; i++) {
                    var kota = visitors[i].kota;
                    
                    var colNum = $("<td/>");
                    colNum.text(i+1).html();
                    var colKota = $("<td/>");
                    colKota.text(kota).html();
                    
                    var row = $("<tr/>");
                    row.append(colNum);
                    row.append(colKota);

                    table.append(row);
                }
            }
        });
    }
});
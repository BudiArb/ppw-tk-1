from django import forms
from main.models import Visitor

class VisitorForm(forms.ModelForm):
    class Meta:
        model = Visitor
        fields = '__all__'
        labels = {
            'kota' : '',
        }
        widgets = {
            'kota' : forms.TextInput(attrs={
                'class' : 'form-input w-100',
                'placeholder' : 'Masukkan kotamu di sini!'
            }),
        }
from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from main.views import add_visitor, visitor_log
from main.models import Visitor
import json
# from selenium import webdriver

# coverage run --include="main/*" --omit="manage.py,ppwtk1" manage.py test

# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_add_visitor(self):
        response = self.client.post('/add_visitor', {'kota' : 'teskota'})
        banyak_kota = Visitor.objects.all().count()
        self.assertEqual(banyak_kota, 1)

    def test_add_visitor_get(self):
        response = self.client.get('/add_visitor')
        self.assertEqual(response.status_code, 302)

    def test_visitor_log(self):
        dummy = self.client.post('/add_visitor', {'kota' : 'teskota'})
        response = self.client.get('/visitor_log.json')
        data = json.loads(response.content)
        self.assertEqual(data['visitors'][0]['kota'], 'teskota')


# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())

from django.shortcuts import render, redirect
from django.http import JsonResponse
from main.models import Visitor
from main.forms import VisitorForm

def home(request):
    form = VisitorForm()
    return render(request, 'main/home.html', {'form' : form})

def visitor_log(request):
    data = {'visitors' : list(Visitor.objects.all().values())}
    return JsonResponse(data, safe=False)

def add_visitor(request):
    if request.method == 'POST':
        form = VisitorForm(request.POST)
        status = 'fail'
        if form.is_valid():
            form.save()
            status = 'success'
        return JsonResponse({'status' : status})
    return redirect('/')
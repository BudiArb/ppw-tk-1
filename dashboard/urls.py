from django.urls import path
from . import views

app_name = "dashboard"

urlpatterns = [
    path('', views.indexDashboard, name='indexDashboard'),
    path('hapus-jadwal/<str:url>', views.hapusJadwal, name='hapusJadwal'),
]
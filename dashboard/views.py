from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from jadwal.models import Jadwal
from userhandle.models import MyUser
import datetime

@login_required
def indexDashboard(request):
    schedules = []
    owners = []
    user_counts = []
    for jadwal in Jadwal.objects.all():
        if request.user in jadwal.users.all():
            schedules.append(jadwal)
            owners.append(MyUser.objects.get(pk=jadwal.owner_id))
            user_counts.append(jadwal.users.all().count())
    schedules_zip = zip(schedules, owners, user_counts)
    if schedules:
        ada_jadwal = True
    else:
        ada_jadwal = False
    return render(request, 'dashboard.html', {'schedules': schedules_zip, 'ada_jadwal': ada_jadwal})

@login_required
def hapusJadwal(request, url):
    if (request.method == 'POST'):
        jadwal = get_object_or_404(Jadwal, url=url)
        messages.success(request, f'Jadwal {jadwal} has been deleted!')
        jadwal.delete()
    return HttpResponseRedirect('/dashboard')

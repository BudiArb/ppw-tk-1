from django.test import TestCase
from datetime import datetime
from userhandle.models import MyUser
from jadwal.models import Jadwal

#coverage run --include="dashboard/*" --omit="main,manage.py,ppwtk1,komentar,jadwal,umpanbalik,userhandle/*" manage.py test

class KomentarTestCase(TestCase):
    def test_dashboard_is_exist_if_user_is_logged_in(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()
        jadwal = Jadwal.objects.create(judul="bermain", tanggal_akhir=datetime.now(), tanggal_awal=datetime.now(), owner_id=user.pk)
        jadwal.users.add(user)
        jadwal.url = "random-url"
        jadwal.save()

        self.client.login(username="test", password="test123")
        response = self.client.get('/dashboard/')
        self.assertIn('bermain', response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'dashboard.html')

    def test_with_no_available_jadwal(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()

        self.client.login(username="test", password="test123")
        response = self.client.get('/dashboard/')
        self.assertNotIn('bermain', response.content.decode('utf8'))
        self.assertIn('Jadwal Tidak Ada', response.content.decode('utf8'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'dashboard.html')

    def test_dashboard_redirects_if_user_is_not_logged_in(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()
        response = self.client.get('/dashboard/')
        self.assertEqual(response.status_code, 302)

    def test_hapusJadwal_is_successful(self):
        user = MyUser.objects.create(username='test', nama_panjang='TestTest')
        user.set_password('test123')
        user.save()
        jadwal = Jadwal.objects.create(judul="bermain", tanggal_akhir=datetime.now(), tanggal_awal=datetime.now(), owner_id=user.pk)
        jadwal.users.add(user)
        jadwal.url = "random-url"
        jadwal.save()
        self.assertEqual(Jadwal.objects.all().count(), 1)

        self.client.login(username="test", password="test123")
        response = self.client.post('/dashboard/hapus-jadwal/' + jadwal.url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Jadwal.objects.all().count(), 0)
        response = self.client.get('/dashboard/')
        self.assertIn('Jadwal bermain has been deleted!', response.content.decode('utf8'))
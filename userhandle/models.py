from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)

class UserManager(BaseUserManager):
    def create_user(self, username, nama_panjang, password):
        if not username:
            raise ValueError('Users must have a username')
        if not nama_panjang:
            raise ValueError('Users must have a name')
        user = self.model(
            username = username,
            nama_panjang = nama_panjang,
        )
        user.set_password(password)
        user.save(using = self._db)
        return user
        
    def create_superuser(self, username, nama_panjang, password):
        user = self.create_user(username, nama_panjang = nama_panjang, password = password)
        user.is_admin = True
        user.save(using = self._db)
        return user

class MyUser(AbstractBaseUser):
    username = models.SlugField(max_length=20, unique=True, db_index=True)
    nama_panjang = models.CharField(max_length=40)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    
    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['nama_panjang']

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

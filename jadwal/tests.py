from django.test import LiveServerTestCase, TestCase, Client, tag
from django.urls import reverse
from django.contrib.auth import get_user
from userhandle.models import MyUser
from jadwal.models import Jadwal, Kegiatan
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import datetime, time, os

@tag('functional')
class JadwalFunctionalTestCases(LiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        options = webdriver.chrome.options.Options()
        options.headless = True
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.driver = webdriver.Chrome(options=options)
        
    def test(self):
        self.index()
        time.sleep(5)
        self.daftar()
        time.sleep(5)
        self.dashboard()
        time.sleep(5)
        self.buat_jadwal()
        time.sleep(5)
        self.isi_jadwal()
        
    def index(self):
        self.driver.get(self.live_server_url)
        self.assertIn("JadwalinAja", self.driver.title)
        
    def daftar(self):
        register = self.driver.find_element_by_link_text("Register")
        ActionChains(self.driver).click(register).perform()
        self.assertIn("Register", self.driver.title)
        
        time.sleep(5)
        self.driver.find_element_by_id("username_sign").send_keys("test-user")
        self.driver.find_element_by_id("pass_1").send_keys("test_password")
        self.driver.find_element_by_id("pass_2").send_keys("test_password")
        self.driver.find_element_by_id("nama_panjang_sign").send_keys("Test Username")
        register = self.driver.find_element_by_id("submit-btn")
        ActionChains(self.driver).click(register).perform()
        navbar = self.driver.find_element_by_tag_name("nav")
        self.assertIn("Test Username", navbar.text)
        
    def dashboard(self):
        dashboard = self.driver.find_element_by_link_text("Dashboard")
        ActionChains(self.driver).click(dashboard).perform()
        self.assertIn("Jadwal Saya", self.driver.title)
    
    def buat_jadwal(self):
        buat = self.driver.find_element_by_link_text("Buat Rapat Baru")
        ActionChains(self.driver).click(buat).perform()
        self.assertIn("Buat Jadwal", self.driver.title)
        
        script = '''
           var tanggal_awal = document.getElementsByName("tanggal_awal")[0];
           var tanggal_akhir = document.getElementsByName("tanggal_akhir")[0];
           tanggal_awal.value = "01/01/1970";
           tanggal_akhir.value = "01/01/1970";
        '''
        
        time.sleep(5)
        self.driver.find_element_by_name("judul").send_keys("Test Jadwal")
        self.driver.execute_script(script)
        buat = self.driver.find_element_by_css_selector("input[value='Jadwalkan Rapat']")
        ActionChains(self.driver).click(buat).perform()
        self.assertIn("Test Jadwal", self.driver.title)
        
        nama = self.driver.find_element_by_class_name("nama")
        self.assertIn("Test Username", nama.text)
    
    def isi_jadwal(self):
        isi = self.driver.find_element_by_link_text("Isi Jadwal")
        ActionChains(self.driver).click(isi).perform()
        self.assertIn("Test Jadwal", self.driver.title)
        
        time.sleep(5)
        
        namas = self.driver.find_elements_by_class_name("nama")
        self.assertIn("Test Username", namas[0].text)
        
        bloks = self.driver.find_elements_by_class_name("kegiatan")
        for blok in bloks:
            ActionChains(self.driver).click(blok).perform()
            time.sleep(1)
            
        bloks = self.driver.find_elements_by_class_name("kegiatan")
        for blok in bloks:
            self.assertIn("1 orang bisa", blok.text)
            
        simpan = self.driver.find_element_by_link_text("Simpan Jadwal")
        ActionChains(self.driver).click(simpan).perform()
        self.assertIn("Test Jadwal", self.driver.title)
            
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()
        super().tearDownClass()
        
@tag('unit')
class JadwalUnitTestCases(TestCase):
    # Client() non-auth user; self.client and non-static Client() auth user
    def setUp(self):
        self.client.post(reverse("userhandle:register"), {'username' : 'test-user', 'password1' : 'test_pass', 'password2' : 'test_pass', 'nama_panjang' : 'Test User'})
        
        self.client.post(reverse("jadwal:buat-jadwal"), {'judul' : 'Test Jadwal', 'tanggal_awal' : '01/01/1970', 'tanggal_akhir' : '01/01/1970'})
        
    def test_non_auth_buat_jadwal_redirects(self):
        response = Client().get(reverse("jadwal:buat-jadwal"), follow=True)
        QUERY_STRING = response.request['QUERY_STRING']
        
        self.assertRedirects(response, reverse("userhandle:login") + '?' + QUERY_STRING)
        
    def test_buat_jadwal_200(self):
        response = self.client.get(reverse("jadwal:buat-jadwal"))
        
        self.assertEqual(response.status_code, 200)
        
    def test_buat_jadwal_form(self):
        response = self.client.post(reverse("jadwal:buat-jadwal"), {'judul' : 'Tes Jadwal', 'tanggal_awal' : '01/01/1970', 'tanggal_akhir' : '01/01/1970'}, follow=True)
        url = Jadwal.objects.get(judul='Tes Jadwal').url
        
        self.assertTrue(Jadwal.objects.filter(url=url).exists())
        self.assertRedirects(response, reverse("jadwal:jadwal", args=[url]))
        self.assertIn("Test User", response.content.decode('utf8'))
        
    def test_buat_jadwal_invalid_form(self):
        response = self.client.post(reverse("jadwal:buat-jadwal"), {'judul' : 'Tes Jadwal', 'tanggal_awal' : '02/01/1970', 'tanggal_akhir' : '01/01/1970'})
        
        self.assertFalse(Jadwal.objects.filter(judul='Tes Jadwal').exists())
        
    def test_jadwal_200(self):
        jadwal = Jadwal.objects.get(judul='Test Jadwal')
        response = self.client.get(reverse("jadwal:jadwal", args=[jadwal.url]))
        
        self.assertEqual(response.status_code, 200)
        self.assertIn(str(jadwal), response.content.decode('utf8'))
    
    def test_non_auth_accept_invite_redirects(self):
        # Anonymous User must login when accepting invitation
        client = Client()
        url = Jadwal.objects.get(judul='Test Jadwal').url
        client.get(reverse("jadwal:invite", args=[url]))
        response = client.get(reverse("jadwal:invite", args=[url]), {'join' : True}, follow=True)
        QUERY_STRING = response.request['QUERY_STRING']
        
        self.assertRedirects(response, reverse("userhandle:login") + '?' + QUERY_STRING)
    
    def test_invite_200(self):
        # Anonymous User and Registered User can access
        jadwal = Jadwal.objects.get(judul='Test Jadwal')
        response = self.client.get(reverse("jadwal:invite", args=[jadwal.url]))
        
        self.assertEqual(response.status_code, 200)
        self.assertIn(jadwal.judul, response.content.decode('utf8'))
        
        response = Client().get(reverse("jadwal:invite", args=[jadwal.url]))
        
        self.assertEqual(response.status_code, 200)
        self.assertIn(str(jadwal), response.content.decode('utf8'))
        
    def test_accept_invite(self):
        client = Client()
        client.post(reverse("userhandle:register"), {'username' : 'test-user-lain', 'password1' : 'test_pass_lain', 'password2' : 'test_pass_lain', 'nama_panjang' : 'Test User Lain'})
        url = Jadwal.objects.get(judul='Test Jadwal').url
        client.get(reverse("jadwal:invite", args=[url]))
        response = client.get(reverse("jadwal:invite", args=[url]), {'join' : True}, follow=True)
        
        self.assertRedirects(response, reverse("jadwal:jadwal", args=[url]))
        self.assertIn("Test User Lain", response.content.decode('utf8'))
        
    def test_non_auth_isi_jadwal_redirects(self):
        url = Jadwal.objects.get(judul='Test Jadwal').url
        response = Client().get(reverse("jadwal:isi-jadwal", args=[url]), follow=True)
        QUERY_STRING = response.request['QUERY_STRING']
        
        self.assertRedirects(response, reverse("userhandle:login") + '?' + QUERY_STRING)
        
    def test_isi_jadwal_200(self):
        url = Jadwal.objects.get(judul='Test Jadwal').url
        response = self.client.get(reverse("jadwal:isi-jadwal", args=[url]))
        
        self.assertEqual(response.status_code, 200)
        
    def test_toggle_jadwal_admin_200(self):
        MyUser.objects.create_superuser(username='test_admin', password='test_password_admin', nama_panjang="Test Admin")
        
        client = Client()
        client.post(reverse("userhandle:login"), {'username' : 'test_admin', 'password' : 'test_password_admin'})
        jadwal = Jadwal.objects.get(judul='Test Jadwal')
        kegiatans = Kegiatan.objects.filter(jadwal=jadwal)
        bisa_cnt = Kegiatan.objects.filter(jadwal=jadwal, bisa=True).count()
        
        for k in kegiatans:
            response = client.get(reverse("jadwal:toggle-jadwal"), {'kegiatan' : k.id})
            
            self.assertEqual(response.status_code, 200)
            
        self.assertEqual(kegiatans.count() - bisa_cnt, Kegiatan.objects.filter(jadwal=jadwal, bisa=True).count())
    
    def test_toggle_jadwal_non_admin_403(self):
        jadwal = Jadwal.objects.get(judul='Test Jadwal')
        kegiatans = Kegiatan.objects.filter(jadwal=jadwal)
        bisa_cnt = Kegiatan.objects.filter(jadwal=jadwal, bisa=True).count()
        
        for k in kegiatans:
            response = self.client.get(reverse("jadwal:toggle-jadwal"), {'kegiatan' : k.id})
            
            self.assertEqual(response.status_code, 403)
            
        self.assertEqual(bisa_cnt, Kegiatan.objects.filter(jadwal=jadwal, bisa=True).count())

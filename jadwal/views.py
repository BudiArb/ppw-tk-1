from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseForbidden
from django.urls import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from .forms import JadwalForm, IsiJadwalForm
from .models import Jadwal, Tanggal, Kegiatan
from userhandle.models import MyUser
from babel.dates import format_date
import string, secrets, datetime

BASE64 = []
TIMESTAMPS = []

def init():
    global BASE64
    global TIMESTAMPS
    
    BASE64 = list(string.ascii_uppercase + string.ascii_lowercase + string.digits + '-' + '_')
    
    for i in range(8, 22):
        TIMESTAMPS.append(datetime.time(hour=i).strftime('%H:%M'))

def date_range(awal, akhir):
    for day_count in range(int((akhir - awal).days) + 1):
        yield awal + datetime.timedelta(day_count)
        
init()

### BUAT JADWAL ###
@login_required
def buat_jadwal(request):
    context = {}
    
    jadwal_form = JadwalForm(request.POST or None)
    
    if request.method == "POST":
        if jadwal_form.is_valid():
            owner = MyUser.objects.get(username=request.user)
            if owner:
                if owner.is_active:
                    ''' PROCESS JADWAL '''
                    jadwal = jadwal_form.save(commit=False)
                    
                    jadwal.owner_id = owner.id
                    
                    random = secrets.SystemRandom()
                    while True:
                        try:
                            url = ''.join(random.sample(BASE64, 40))
                            Jadwal.objects.get(url=url)
                        except Jadwal.DoesNotExist:
                            break
                    jadwal.url = url
                    
                    jadwal.save()
                    jadwal.users.add(owner)

                    ''' PROCESS TANGGAL & KEGIATAN '''     
                    dates = []
                    awal = jadwal.tanggal_awal
                    akhir = jadwal.tanggal_akhir
                    for date in date_range(awal, akhir):
                        tanggal = Tanggal(jadwal=jadwal, tanggal=date) 
                        tanggal.save()
                        
                        for hour in range(8, 21):
                            kegiatan = Kegiatan(user=owner, jadwal=jadwal, tanggal=tanggal, jam_awal=datetime.time(hour=hour))
                            kegiatan.save()
                    
                    return HttpResponseRedirect(reverse("jadwal:jadwal", args=[url]))
        else:
            context['jadwal_form'] = jadwal_form
    else:
        context['jadwal_form'] = JadwalForm()
    
    return render(request, 'jadwal/buat-jadwal.html', context)
 
### INVITE ### 
def invite(request, url):
    context = {}
    
    jadwal = Jadwal.objects.get(url=url)
    context['jadwal'] = jadwal      
    
    if request.GET.get('join', False):
        return accept_invite(request, jadwal.url)
            
    return render(request, 'jadwal/invite.html', context)

### JADWAL ###
@login_required
def jadwal(request, url):
    context = {}

    context['TS'] = TIMESTAMPS
    
    jadwal = Jadwal.objects.get(url=url)
    context['jadwal'] = jadwal
    
    owner = MyUser.objects.get(id=jadwal.owner_id)
    context['owner'] = owner
    
    users = []
    users = filter(lambda user: user != owner, jadwal.users.all())
    context['users'] = users
    
    user = MyUser.objects.get(username=request.user)
    if user:
        if user.is_active:
            dates = dict()
            for date in Tanggal.objects.filter(jadwal=jadwal):
                tanggal = format_date(date.tanggal, "EEEE, dd/MM/yy", locale="id")
                kegiatan_count = []
                for hour in range(8, 21):
                    kegiatan_count.append(Kegiatan.objects.filter(jadwal=jadwal, tanggal=date, bisa=True, jam_awal=datetime.time(hour=hour)).count())
                dates[tanggal] = kegiatan_count
            context['dates'] = dates
            
    return render(request, 'jadwal/jadwal.html', context)
  
### ISI JADWAL ###  
@login_required
def isi_jadwal(request, url):
    context = {}
    
    context['TS'] = TIMESTAMPS
    
    jadwal = Jadwal.objects.get(url=url)
    context['jadwal'] = jadwal
    
    owner = MyUser.objects.get(id=jadwal.owner_id)
    context['owner'] = owner
    
    users = filter(lambda user: user != owner, jadwal.users.all())
    context['users'] = users
    
    user = MyUser.objects.get(username=request.user)
    if user and user.is_active:
        dates = {}
        for date in Tanggal.objects.filter(jadwal=jadwal):
            tanggal = format_date(date.tanggal, "EEEE, dd/MM/yy", locale="id")
            kegiatan = []
            for hour in range(8, 21):
                kegiatan_count = Kegiatan.objects.filter(jadwal=jadwal, tanggal=date, bisa=True, jam_awal=datetime.time(hour=hour)).count()
                my_kegiatan = Kegiatan.objects.get(user=user, jadwal=jadwal, tanggal=date, jam_awal=datetime.time(hour=hour))
                
                form_prefix = str(my_kegiatan.id)
                my_kegiatan_form = IsiJadwalForm(prefix=form_prefix, initial={'bisa' : my_kegiatan.bisa})
                
                kegiatan.append((kegiatan_count, my_kegiatan_form))
            dates[tanggal] = kegiatan
        context['dates'] = dates
            
    return render(request, 'jadwal/isi-jadwal.html', context)

### Handle input dari AJAX ###
def toggle_kegiatan(request):
    if request.META.get('HTTP_REFERER', False) or request.user.is_admin:
        kegiatan_id = int(request.GET["kegiatan"])
        next_bisa = not Kegiatan.objects.get(id=kegiatan_id).bisa
        Kegiatan.objects.filter(id=kegiatan_id).update(bisa=next_bisa)
                
        return HttpResponse()
    else:
        return HttpResponseForbidden()
    
### Handle accept invite ###
@login_required
def accept_invite(request, url):
    jadwal = Jadwal.objects.get(url=url)
    user = request.user
    try:
        jadwal.users.get(username=user.username)
    except MyUser.DoesNotExist:
        if user.id != jadwal.owner_id:
            jadwal.users.add(user)
            
            awal = jadwal.tanggal_awal
            akhir = jadwal.tanggal_akhir
            for date in date_range(awal, akhir):
                tanggal = Tanggal.objects.get(jadwal=jadwal, tanggal=date)
                
                for hour in range(8, 21):
                    kegiatan = Kegiatan(user=user, jadwal=jadwal, tanggal=tanggal, jam_awal=datetime.time(hour=hour))
                    kegiatan.save()
    
    return HttpResponseRedirect(reverse("jadwal:jadwal", args=[jadwal.url]))
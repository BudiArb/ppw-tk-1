from django import forms
from django.core.exceptions import ValidationError
from jadwal.models import Jadwal, Kegiatan

class JadwalForm(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = ['judul', 'tanggal_awal', 'tanggal_akhir']
        
    judul = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                    'class' : 'form-input',
                    'placeholder' : 'Judul Rapat',
                    'required' : 'required'
                }
    ))
    
    tanggal_awal = forms.DateField(label="", input_formats=["%d/%m/%Y"], widget=forms.DateInput(
        attrs = {
                    'type' : 'text',
                    'class' : 'form-input',
                    'placeholder' : 'Tanggal awal',
                    'required' : 'required'
                }
    ))
    
    tanggal_akhir = forms.DateField(label="", input_formats=["%d/%m/%Y"], widget=forms.DateInput(
        attrs = {
                    'type' : 'text',
                    'class' : 'form-input',
                    'placeholder' : 'Tanggal akhir',
                    'required' : 'required'
                }
    ))
    
class AnggotaForm(forms.Form):
    nama = forms.CharField(label="", widget=forms.TextInput(
        attrs = {
                    'class' : 'form-input',
                    'placeholder' : 'Tambah Anggota'
                }
    ))
    
class IsiJadwalForm(forms.Form):
    bisa = forms.BooleanField(label="", widget=forms.CheckboxInput(attrs = {'hidden' : True}))
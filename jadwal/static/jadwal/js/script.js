// $(function({})) = $(document).ready(function())
$(function(){
    // Exact height
    var navHeight = $("nav").outerHeight();
    $("section").css("min-height", "calc(100vh - " + navHeight + "px)");
    
    var timestampHeight = $(".timestamp").outerHeight();
    var timestampsHeight = $(".timestamps").height();
    var goodPadding = timestampHeight/2;
    $(".blocks").css("min-height", timestampsHeight + "px");
    $(".blocks").css("padding", goodPadding + "px 1vw");
    
    $("input[type='checkbox']").prop("required", false);
    
    var tanggalAwal = $("input[name='tanggal_awal']");
    var tanggalAkhir = $("input[name='tanggal_akhir']");
    tanggalAwal.on("focus", dateInputFocus);
    tanggalAwal.on("blur", dateInputBlur);
    tanggalAkhir.on("focus", dateInputFocus);
    tanggalAkhir.on("blur", dateInputBlur);
    
    $(".kegiatan").each(function(){
        $(this).on("click", checkboxHandler)
    });
});

// Kalau pencet pilih tanggal, gak bikin lebarnya jadi kecil
function dateInputFocus(){
    var x = $(this);
    var placeholderWidth = x.width();
    x.attr("type", "date");
    x.width(placeholderWidth);
}

// Unset
function dateInputBlur(){
    var x = $(this);
    x.attr("type", "text");
    var tanggal = x.val().split("-").reverse().map(function(s){
        return parseInt(s);
    });
    if(tanggal.length > 1){
        var hari = new Date(x.val());
        hari = hari.getDay();
        x.val(tanggal.join("/"));
    }
}

// Copy ke clipboard, bikin textarea sekejap, isi link, terus di-copy
function copyInvitationalLink(domain, url){
    var x = document.createElement("textarea");
    x.value = domain + url;
    $("body").append(x);
    x.select();
    document.execCommand("copy");
    $(x).remove();
    
    $("#copy-success").removeClass("invisible");
    $("#copy-success").addClass("fade-out");
    setTimeout(function(){
        $("#copy-success").removeClass("fade-out");
        $("#copy-success").addClass("invisible");
    }, 2000);
}

// Handle input isi jadwal
function checkboxHandler(){
    var x = $(this);
    var checkboxInput = x.find("input[type='checkbox']");
    var id = checkboxInput.prop("name").split("-")[0];
    
    $.ajax({
        url: "/jadwal/toggle-kegiatan",
        data: {"kegiatan" : id},
        success: function(data){
            var checked = checkboxInput.prop("checked");   
            var bisaCount = parseInt($("#counter", x).text());
            
            if((!checked && bisaCount == 0) || (checked && bisaCount == 1)){
                x.toggleClass("bg-blue");
                $("div", x).toggleClass("invisible");
            }
            
            bisaCount = bisaCount + (checked ? -1 : 1);
            $("span > #counter", x).text(bisaCount);
            $(checkboxInput).prop("checked", !checked); 
        }
    });
}
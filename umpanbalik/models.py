from django.db import models

# Create your models here.
class Feedback(models.Model):
    """docstring for Feedback."""
    comment = models.TextField()
    createDate = models.DateTimeField(auto_now_add=True, blank=True)
from django.test import TestCase
from datetime import datetime
from .models import Feedback

class FeedbackTestCase(TestCase):
    def test_feedback_create_is_exist(self):
        response = self.client.get('/feedback/create')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'feedback.html')

    def test_feedback_create_is_success(self):
        response = self.client.post('/feedback/create', {'comment': 'Ini contoh feedback'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(Feedback.objects.all().count(), 1)
        
        response = self.client.get('/feedback/list')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Ini contoh feedback', response.content.decode('utf8'))
        self.assertTemplateUsed(response, 'feedbacklist.html')

    def test_feedback_edit_is_success(self):
        test_feedback = Feedback.objects.create(comment="Ini contoh feedback", createDate=datetime.now())
        response = self.client.post('/feedback/edit/' + str(test_feedback.id), {'comment': 'Sudah diedit'})
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/feedback/list')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Sudah diedit', response.content.decode('utf8'))
        self.assertNotIn('Ini contoh feedback', response.content.decode('utf8'))
        self.assertTemplateUsed(response, 'feedbacklist.html')